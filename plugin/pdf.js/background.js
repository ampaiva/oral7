
var websocket;
function createWebSocketConnection(host) {

  if ('WebSocket' in window) {
    websocket = new WebSocket(host);
    console.log("======== websocket ===========", websocket);

    websocket.onopen = function () {
      websocket.send("Hello");
    };

    websocket.onmessage = function (event) {
      console.log('received: '+ event);
      var received_msg = JSON.parse(event.data);
      var notificationOptions = {
        type: "basic",
        title: received_msg.title,
        message: received_msg.message,
        iconUrl: "extension-icon.png"
      }
      chrome.notifications.create("", notificationOptions);
    };

    websocket.onerror = function (e) {
      alert('==== web socket error ======' + e);
    };

    websocket.onclose = function () {
      alert("==== web socket closed======");
    };
  }
}

function postMessage (msg) {
    console.log('post: '+ msg);
    createWebSocketConnection("ws://localhost:8081");
}
